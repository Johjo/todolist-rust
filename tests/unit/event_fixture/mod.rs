use serde::{Deserialize, Serialize};
use todolist::domain::event_store::EventContent;

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum TestEvent {
    SomeEvent { data: String },
    OtherEvent { data: String },
}

impl EventContent for TestEvent {}

impl TestEvent {
    pub fn new_some_event(data: &str) -> TestEvent {
        TestEvent::SomeEvent {
            data: data.to_string(),
        }
    }

    pub fn new_other_event(data: &str) -> TestEvent {
        TestEvent::OtherEvent {
            data: data.to_string(),
        }
    }
}
