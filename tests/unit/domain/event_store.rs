use spectral::prelude::*;
use std::sync::Arc;

use crate::event_fixture::TestEvent;
use crate::event_fixture::TestEvent::OtherEvent;
use crate::event_fixture::TestEvent::SomeEvent;
use todolist::domain::event_store::{
    AggregateId, AggregateVersion, Event, EventStore, EventStoreError,
};
use todolist::infra::event_storage_in_memory::StorageInMemory;

struct Context {
    event_store: Arc<EventStore<TestEvent>>,
}

impl Context {
    fn new() -> Context {
        let storage = Arc::new(StorageInMemory::new());
        let event_store = Arc::new(EventStore::new(storage));

        Context { event_store }
    }

    fn when_append_event(
        &self,
        aggregate_id: &str,
        event: Event<TestEvent>,
    ) -> Result<(), EventStoreError> {
        self.event_store
            .append_event(AggregateId::new(aggregate_id), event)
    }

    fn get_history(&self, aggregate_id: &str) -> Vec<Event<TestEvent>> {
        self.event_store.list_events(AggregateId::new(aggregate_id))
    }
}

#[test]
fn should_store_some_event() {
    let context = Context::new();

    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_some_event("some data"),
            },
        )
        .unwrap();

    assert_that!(context.get_history("the-aggregate-id")).contains(Event {
        version: AggregateVersion::from_u32(1),
        content: SomeEvent {
            data: String::from("some data"),
        },
    });
}

#[test]
fn should_store_many_events() {
    let context = Context::new();

    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_some_event("some data"),
            },
        )
        .unwrap();
    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(2),
                content: TestEvent::new_some_event("other data"),
            },
        )
        .unwrap();

    assert_that!(context.get_history("the-aggregate-id")).is_equal_to(vec![
        Event {
            version: AggregateVersion::from_u32(1),
            content: SomeEvent {
                data: String::from("some data"),
            },
        },
        Event {
            version: AggregateVersion::from_u32(2),
            content: SomeEvent {
                data: String::from("other data"),
            },
        },
    ]);
}

#[test]
fn should_store_other_event() {
    let context = Context::new();

    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_other_event("some data"),
            },
        )
        .unwrap();

    assert_that!(context.get_history("the-aggregate-id")).contains(Event {
        version: AggregateVersion::from_u32(1),
        content: OtherEvent {
            data: String::from("some data"),
        },
    });
}

#[test]
fn should_success_when_add_event_to_event_store() {
    let context = Context::new();

    let result = context.when_append_event(
        "the-aggregate-id",
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_some_event("some data"),
        },
    );

    assert_that!(result).is_ok();
}

#[test]
fn should_fail_with_conflict_version_when_two_event_has_same_version() {
    let context = Context::new();

    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_some_event("some data"),
            },
        )
        .unwrap();

    let result = context.when_append_event(
        "the-aggregate-id",
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_some_event("other data"),
        },
    );

    assert_that!(result)
        .is_err()
        .is_equal_to(EventStoreError::ConflictVersion);
}

#[test]
fn should_fail_with_inconsistent_version_when_version_does_not_start_to_one() {
    let context = Context::new();

    let result = context.when_append_event(
        "the-aggregate-id",
        Event {
            version: AggregateVersion::from_u32(2),
            content: TestEvent::new_some_event("some data"),
        },
    );

    assert_that!(result)
        .is_err()
        .is_equal_to(EventStoreError::InconsistentVersion);
}

#[test]
fn should_fail_with_inconsistent_version_when_version_does_not_follow() {
    let context = Context::new();

    context
        .when_append_event(
            "the-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_some_event("some data"),
            },
        )
        .unwrap();

    let result = context.when_append_event(
        "the-aggregate-id",
        Event {
            version: AggregateVersion::from_u32(3),
            content: TestEvent::new_some_event("other data"),
        },
    );

    assert_that!(result)
        .is_err()
        .is_equal_to(EventStoreError::InconsistentVersion);
}

#[test]
fn should_version_be_checked_for_a_same_aggregate() {
    let context = Context::new();

    context
        .when_append_event(
            "some-aggregate-id",
            Event {
                version: AggregateVersion::from_u32(1),
                content: TestEvent::new_other_event("some data"),
            },
        )
        .unwrap();

    let result = context.when_append_event(
        "other-aggregate-id",
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_other_event("other data"),
        },
    );

    assert_that!(result).is_ok();
}
