use std::sync::{Arc, Mutex};
use todolist::domain::event_store::{Event, EventStore, EventSubscriber};
use todolist::domain::tasklist::error::TasklistError;
use todolist::domain::tasklist::event::TaskEventContent;
use todolist::domain::tasklist::use_case::{
    TaskCompleter, TaskCreator, TaskListCreator, TasklistEventStore,
};
use todolist::infra::event_storage_in_memory::StorageInMemory;

pub struct Context {
    storage: Arc<StorageInMemory>,
    event_store: TasklistEventStore,
}

impl Context {
    pub fn new() -> Context {
        let storage = Arc::new(StorageInMemory::new());
        let event_store = Arc::new(EventStore::new(storage.clone()));
        Context {
            storage,
            event_store,
        }
    }

    pub(crate) fn given_history(&self, aggregate_id: &str, history: Vec<Event<TaskEventContent>>) {
        history
            .into_iter()
            .for_each(|event| self.storage.append_event(aggregate_id.to_string(), event));
    }

    pub(crate) fn given_subscriber(&self, subscriber: Arc<SubscriberSpy>) {
        self.event_store.subscribe(subscriber);
    }

    pub(crate) fn when_create_tasklist(&self, tasklist_id: &str) -> Result<(), TasklistError> {
        let tasklist_creator = TaskListCreator::new(self.event_store.clone());
        tasklist_creator.create_tasklist(tasklist_id)
    }

    pub(crate) fn when_create_task(
        &self,
        tasklist_id: &str,
        task_title: &str,
    ) -> Result<(), TasklistError> {
        let task_creator = TaskCreator::new(self.event_store.clone());
        task_creator.create_task(tasklist_id, task_title)
    }

    pub(crate) fn when_complete_task(
        &self,
        tasklist_id: &str,
        task_id: u32,
    ) -> Result<(), TasklistError> {
        let task_completer = TaskCompleter::new(self.event_store.clone());
        task_completer.complete_task(tasklist_id, task_id)
    }

    pub fn get_history(&self, aggregate_id: &str) -> Vec<Event<TaskEventContent>> {
        self.storage
            .list_event::<TaskEventContent>(aggregate_id.to_string())
    }
}

pub struct SubscriberSpy {
    events: Mutex<Vec<Event<TaskEventContent>>>,
}

impl SubscriberSpy {}

impl EventSubscriber<TaskEventContent> for SubscriberSpy {
    fn handle(&self, event: Event<TaskEventContent>) {
        self.events.lock().unwrap().push(event);
    }
}

impl SubscriberSpy {
    pub(crate) fn new() -> SubscriberSpy {
        SubscriberSpy {
            events: Mutex::new(Vec::new()),
        }
    }

    pub(crate) fn get_history(&self) -> Vec<Event<TaskEventContent>> {
        self.events.lock().unwrap().clone()
    }
}
