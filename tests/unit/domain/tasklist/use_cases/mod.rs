use crate::domain::tasklist::fixture::{Context, SubscriberSpy};

use todolist::domain::tasklist::error::TasklistError;
use todolist::domain::tasklist::event::TaskEventContent;
use todolist::domain::event_store::{AggregateVersion, Event};

use spectral::prelude::*;
use std::sync::Arc;

#[test]
fn should_raise_add_event_tasklist_created_when_create_tasklist_1() {
    let context = Context::new();

    context.when_create_tasklist("tasklist_id").unwrap();

    assert_that!(context.get_history("tasklist_id")).contains(Event {
        version: AggregateVersion::from_u32(1),
        content: TaskEventContent::TaskListCreated {
            tasklist_id: "tasklist_id".to_string(),
        },
    });
}

#[test]
fn should_raise_message_tasklist_created_when_create_tasklist_for_good_tasklist_id() {
    let context = Context::new();

    context.when_create_tasklist("other_tasklist_id").unwrap();

    assert_that!(context.get_history("other_tasklist_id")).contains(Event {
        version: AggregateVersion::from_u32(1),
        content: TaskEventContent::TaskListCreated {
            tasklist_id: "other_tasklist_id".to_string(),
        },
    });
}

#[test]
fn should_raise_error_when_create_twice_tasklist_with_same_tasklist_id_twice() {
    let context = Context::new();
    context.when_create_tasklist("some_tasklist_id").unwrap();

    let result = context.when_create_tasklist("some_tasklist_id");

    assert_that!(result)
        .is_err()
        .is_equal_to(TasklistError::TaskListAlreadyExists);
}

#[test]
fn should_not_raise_message_tasklist_created_when_create_tasklist_twice() {
    let context = Context::new();
    context.when_create_tasklist("other_tasklist_id").unwrap();

    context
        .when_create_tasklist("other_tasklist_id")
        .unwrap_err();

    assert_that!(context.get_history("other_tasklist_id")).is_equal_to(vec![Event {
        version: AggregateVersion::from_u32(1),
        content: TaskEventContent::TaskListCreated {
            tasklist_id: "other_tasklist_id".to_string(),
        },
    }]);
}

#[test]
fn should_publish_message_tasklist_created_when_create_tasklist() {
    let context = Context::new();

    let subscriber = Arc::new(SubscriberSpy::new());
    context.given_subscriber(subscriber.clone());

    context.when_create_tasklist("some_tasklist_id").unwrap();

    assert_that!(subscriber.get_history()).contains(Event {
        version: AggregateVersion::from_u32(1),
        content: TaskEventContent::TaskListCreated {
            tasklist_id: "some_tasklist_id".to_string(),
        },
    });
}

#[test]
fn should_raise_error_tasklist_does_not_exist_when_create_task_in_inexisting_tasklist() {
    let context = Context::new();

    let result = context.when_create_task("tasklist_id", "some task");

    assert_that!(result)
        .is_err()
        .is_equal_to(TasklistError::TasklistDoesNotExist);
}

#[test]
fn should_raise_message_task_created_when_create_task() {
    let context = Context::new();
    context.given_history(
        "tasklist_id",
        vec![Event {
            version: AggregateVersion::from_u32(1),
            content: TaskEventContent::TaskListCreated {
                tasklist_id: "tasklist_id".to_string(),
            },
        }],
    );

    context
        .when_create_task("tasklist_id", "some task")
        .unwrap();

    assert_that!(context.get_history("tasklist_id")).contains(Event {
        version: AggregateVersion::from_u32(2),
        content: TaskEventContent::TaskCreated {
            task_id: 1,
            title: "some task".to_string(),
        },
    });
}

#[test]
fn should_increase_event_version_when_raise_message() {
    let context = Context::new();
    context.given_history(
        "tasklist_id",
        vec![Event {
            version: AggregateVersion::from_u32(1),
            content: TaskEventContent::TaskListCreated {
                tasklist_id: "tasklist_id".to_string(),
            },
        }],
    );

    context
        .when_create_task("tasklist_id", "some task")
        .unwrap();
    context
        .when_create_task("tasklist_id", "other task")
        .unwrap();

    assert_that!(context
        .get_history("tasklist_id")
        .into_iter()
        .map(|event| event.version)
        .collect::<Vec<AggregateVersion>>())
    .is_equal_to(vec![
        AggregateVersion::from_u32(1),
        AggregateVersion::from_u32(2),
        AggregateVersion::from_u32(3),
    ]);
}

#[test]
fn should_raise_task_id_when_create_task() {
    let context = Context::new();
    context.given_history(
        "tasklist_id",
        vec![
            Event {
                version: AggregateVersion::from_u32(1),
                content: TaskEventContent::TaskListCreated {
                    tasklist_id: "tasklist_id".to_string(),
                },
            },
            Event {
                version: AggregateVersion::from_u32(2),
                content: TaskEventContent::TaskCreated {
                    task_id: 1,
                    title: "some task".to_string(),
                },
            },
        ],
    );

    context
        .when_create_task("tasklist_id", "other task")
        .unwrap();

    assert_that!(context.get_history("tasklist_id")).contains(Event {
        version: AggregateVersion::from_u32(3),
        content: TaskEventContent::TaskCreated {
            task_id: 2,
            title: "other task".to_string(),
        },
    });
}

#[test]
fn should_raise_task_completed_when_task_is_done() {
    let context = Context::new();
    context.given_history(
        "tasklist_id",
        vec![
            Event {
                version: AggregateVersion::from_u32(1),
                content: TaskEventContent::TaskListCreated {
                    tasklist_id: "tasklist_id".to_string(),
                },
            },
            Event {
                version: AggregateVersion::from_u32(2),
                content: TaskEventContent::TaskCreated {
                    task_id: 1,
                    title: "some task".to_string(),
                },
            },
        ],
    );

    context.when_complete_task("tasklist_id", 1).unwrap();

    assert_that!(context.get_history("tasklist_id")).contains(Event {
        version: AggregateVersion::from_u32(3),
        content: TaskEventContent::TaskCompleted {
            tasklist_id: "tasklist_id".to_string(),
            task_id: 1,
        },
    });
}

#[test]
fn should_not_raise_message_task_completed_when_task_is_done_twice() {
    let context = Context::new();
    context.given_history(
        "tasklist_id",
        vec![
            Event {
                version: AggregateVersion::from_u32(1),
                content: TaskEventContent::TaskListCreated {
                    tasklist_id: "tasklist_id".to_string(),
                },
            },
            Event {
                version: AggregateVersion::from_u32(2),
                content: TaskEventContent::TaskCreated {
                    task_id: 1,
                    title: "some task".to_string(),
                },
            },
            Event {
                version: AggregateVersion::from_u32(3),
                content: TaskEventContent::TaskCompleted {
                    tasklist_id: "tasklist_id".to_string(),
                    task_id: 1,
                },
            },
        ],
    );

    context.when_complete_task("tasklist_id", 1).unwrap();

    assert_that!(context.get_history("tasklist_id")).has_length(3);
}
