use crate::event_fixture::TestEvent;
use crate::event_fixture::TestEvent::SomeEvent;
use todolist::domain::event_store::{AggregateVersion, Event};
use todolist::infra::event_storage_in_memory::StorageInMemory;

#[test]
fn should_be_empty() {
    let storage = StorageInMemory::new();
    assert_eq!(
        storage.list_event::<TestEvent>("some id".to_string()),
        vec![]
    );
}

#[test]
fn should_store_one_event() {
    let storage = StorageInMemory::new();

    storage.append_event(
        "id".to_string(),
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_some_event("some data"),
        },
    );

    assert_eq!(
        storage.list_event::<TestEvent>("id".to_string()),
        vec![Event {
            version: AggregateVersion::from_u32(1),
            content: SomeEvent {
                data: String::from("some data")
            }
        }]
    );
}

#[test]
fn should_store_many_aggregate() {
    let storage = StorageInMemory::new();

    storage.append_event(
        "some_aggregate_id".to_string(),
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_some_event("some data 1"),
        },
    );

    storage.append_event(
        "some_aggregate_id".to_string(),
        Event {
            version: AggregateVersion::from_u32(2),
            content: TestEvent::new_some_event("some data 2"),
        },
    );

    storage.append_event(
        "other_aggregate_id".to_string(),
        Event {
            version: AggregateVersion::from_u32(1),
            content: TestEvent::new_some_event("other data 1 "),
        },
    );

    storage.append_event(
        "other_aggregate_id".to_string(),
        Event {
            version: AggregateVersion::from_u32(2),
            content: TestEvent::new_some_event("other data 2"),
        },
    );

    assert_eq!(
        storage.list_event::<TestEvent>("some_aggregate_id".to_string()),
        vec![
            Event {
                version: AggregateVersion::from_u32(1),
                content: SomeEvent {
                    data: String::from("some data 1")
                }
            },
            Event {
                version: AggregateVersion::from_u32(2),
                content: SomeEvent {
                    data: String::from("some data 2")
                }
            }
        ]
    );

    assert_eq!(
        storage.list_event::<TestEvent>("other_aggregate_id".to_string()),
        vec![
            Event {
                version: AggregateVersion::from_u32(1),
                content: SomeEvent {
                    data: String::from("other data 1 ")
                }
            },
            Event {
                version: AggregateVersion::from_u32(2),
                content: SomeEvent {
                    data: String::from("other data 2")
                }
            }
        ]
    );
}
