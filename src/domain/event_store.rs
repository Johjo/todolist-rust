use crate::infra::event_storage_in_memory::StorageInMemory;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

#[derive(Clone, Eq, Hash, PartialEq)]
pub struct AggregateId(String);

impl AggregateId {
    pub fn new(id: &str) -> AggregateId {
        AggregateId(id.to_string())
    }
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Event<EventContent> {
    pub version: AggregateVersion,
    pub content: EventContent,
}

pub trait EventContent: Serialize + DeserializeOwned {}

pub trait EventSubscriber<EventContent> {
    fn handle(&self, event: Event<EventContent>);
}

#[derive(Debug, PartialEq, Deserialize, Serialize, Clone)]
pub struct AggregateVersion(u32);

impl AggregateVersion {
    pub(crate) fn initial() -> AggregateVersion {
        AggregateVersion(0)
    }

    fn next(&self) -> AggregateVersion {
        AggregateVersion(self.0 + 1)
    }

    pub fn from_u32(version: u32) -> AggregateVersion {
        AggregateVersion(version)
    }
}

pub struct EventStore<EVENT: EventContent>
where
    EVENT: Clone,
{
    storage: Arc<StorageInMemory>,
    subscribers: Mutex<Vec<Arc<dyn EventSubscriber<EVENT>>>>,
    event_type: std::marker::PhantomData<EVENT>,
}

impl<EVENT: EventContent + Clone> EventStore<EVENT> {
    pub fn new(storage: Arc<StorageInMemory>) -> EventStore<EVENT> {
        EventStore {
            storage,
            subscribers: Mutex::new(Vec::new()),
            event_type: PhantomData,
        }
    }

    pub fn appends_many_events(
        &self,
        event_contents: Vec<EVENT>,
        aggregate_id: AggregateId,
    ) -> Result<(), EventStoreError> {
        let current_version = self.get_last_version(aggregate_id.clone());

        for event_content in event_contents {
            self.append_event(
                aggregate_id.clone(),
                Event {
                    version: current_version.next(),
                    content: event_content,
                },
            )?;
        }

        Ok(())
    }

    pub fn append_event(
        &self,
        aggregate_id: AggregateId,
        event: Event<EVENT>,
    ) -> Result<(), EventStoreError> {
        self.check_version_of_event(aggregate_id.clone(), &event)?;

        let AggregateId(aggregate_id) = aggregate_id;

        self.storage.append_event(aggregate_id, event.clone());

        for subscriber in self.subscribers.lock().unwrap().iter() {
            subscriber.handle(event.clone());
        }

        Ok(())
    }

    fn check_version_of_event(
        &self,
        aggregate_id: AggregateId,
        event: &Event<EVENT>,
    ) -> Result<(), EventStoreError> {
        let last_version = self.get_last_version(aggregate_id);

        if last_version == event.version {
            return Err(EventStoreError::ConflictVersion);
        }
        if last_version.next() != event.version {
            return Err(EventStoreError::InconsistentVersion);
        }

        Ok(())
    }

    pub fn get_last_version(&self, aggregate_id: AggregateId) -> AggregateVersion {
        let AggregateId(aggregate_id) = aggregate_id;
        let events: Vec<Event<EVENT>> = self.storage.list_event(aggregate_id);
        let last = events.into_iter().last();

        if let Some(Event { version, .. }) = last {
            version
        } else {
            AggregateVersion::initial()
        }
    }

    pub fn list_events(&self, aggregate_id: AggregateId) -> Vec<Event<EVENT>> {
        self.storage.list_event(aggregate_id.0)
    }

    pub fn subscribe(&self, subscriber: Arc<dyn EventSubscriber<EVENT>>) {
        self.subscribers.lock().unwrap().push(subscriber);
    }
}

#[derive(PartialEq, Debug)]
pub enum EventStoreError {
    ConflictVersion,
    InconsistentVersion,
}
