#[derive(Debug, PartialEq)]
pub enum TasklistError {
    TaskListAlreadyExists,
    TasklistDoesNotExist,
}
