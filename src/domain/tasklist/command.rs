pub(crate) enum TasklistCommand {
    CreateTasklist { tasklist_id: String },
    CreateTask { title: String },
    CompleteTask { tasklist_id: String, task_id: u32 },
}
