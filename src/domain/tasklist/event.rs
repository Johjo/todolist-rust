use crate::domain::event_store::EventContent;
use serde::{Deserialize, Serialize};

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum TaskEventContent {
    TaskListCreated { tasklist_id: String },
    TaskCreated { task_id: u32, title: String },
    TaskCompleted { tasklist_id: String, task_id: u32 },
}

impl EventContent for TaskEventContent {}
