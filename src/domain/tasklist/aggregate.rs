use crate::domain::event_store::{AggregateVersion, Event};
use crate::domain::tasklist::command::TasklistCommand;
use crate::domain::tasklist::error::TasklistError;
use crate::domain::tasklist::event::TaskEventContent;
use std::collections::HashMap;

struct Task {
    completed: bool,
}

struct Tasklist {
    tasks: HashMap<u32, Task>,
}

struct TasklistState {
    tasklist_exist: bool,
    last_task_id: u32,
    tasklist: Tasklist,
    version: AggregateVersion,
}

impl TasklistState {
    fn initial() -> TasklistState {
        TasklistState {
            tasklist_exist: false,
            last_task_id: 0,
            tasklist: Tasklist {
                tasks: HashMap::new(),
            },
            version: AggregateVersion::initial(),
        }
    }

    fn apply(self, event: Event<TaskEventContent>) -> TasklistState {
        let TasklistState {
            mut tasklist_exist,
            mut last_task_id,
            mut tasklist,
            version,
        } = self;

        match event.content {
            TaskEventContent::TaskListCreated { .. } => {
                tasklist_exist = true;
            }

            TaskEventContent::TaskCreated { .. } => {
                last_task_id += 1;
                tasklist
                    .tasks
                    .insert(last_task_id, Task { completed: false });
            }

            TaskEventContent::TaskCompleted { task_id, .. } => {
                tasklist.tasks.insert(task_id, Task { completed: true });
            }
        }

        TasklistState {
            tasklist_exist,
            last_task_id,
            tasklist,
            version,
        }
    }
}

pub(crate) struct TaskListAggregate {
    state: TasklistState,
}

impl TaskListAggregate {
    pub(crate) fn new(events: Vec<Event<TaskEventContent>>) -> TaskListAggregate {
        let state = events
            .into_iter()
            .fold(TasklistState::initial(), |state, event| state.apply(event));

        TaskListAggregate { state }
    }

    pub(crate) fn handle(
        &self,
        command: TasklistCommand,
    ) -> Result<Vec<TaskEventContent>, TasklistError> {
        match command {
            TasklistCommand::CreateTasklist { tasklist_id } => {
                if self.state.tasklist_exist {
                    return Err(TasklistError::TaskListAlreadyExists);
                }
                Ok(vec![TaskEventContent::TaskListCreated {
                    tasklist_id: tasklist_id,
                }])
            }

            TasklistCommand::CreateTask { title } => {
                if !self.state.tasklist_exist {
                    return Err(TasklistError::TasklistDoesNotExist);
                }

                Ok(vec![TaskEventContent::TaskCreated {
                    task_id: self.state.last_task_id + 1,
                    title: title.to_string(),
                }])
            }
            TasklistCommand::CompleteTask {
                tasklist_id,
                task_id,
                ..
            } => {
                if self.state.tasklist.tasks.get(&task_id).unwrap().completed {
                    return Ok(vec![]);
                }
                Ok(vec![TaskEventContent::TaskCompleted {
                    tasklist_id,
                    task_id,
                }])
            }
        }
    }
}
