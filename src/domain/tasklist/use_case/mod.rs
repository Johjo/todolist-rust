use crate::domain::event_store::EventStore;
use crate::domain::tasklist::command::TasklistCommand;
use crate::domain::tasklist::error::TasklistError;
use crate::domain::tasklist::event::TaskEventContent;
use crate::domain::tasklist::service::TasklistService;
use std::sync::Arc;

pub type TasklistEventStore = Arc<EventStore<TaskEventContent>>;

pub struct TaskListCreator {
    event_store: TasklistEventStore,
}

impl TaskListCreator {
    pub fn new(event_store: TasklistEventStore) -> TaskListCreator {
        TaskListCreator { event_store }
    }

    pub fn create_tasklist(&self, tasklist_id: &str) -> Result<(), TasklistError> {
        let command = TasklistCommand::CreateTasklist {
            tasklist_id: tasklist_id.to_string(),
        };

        let tasklist = TasklistService::new();
        tasklist.handle(tasklist_id, command, &self.event_store)
    }
}

pub struct TaskCreator {
    event_store: TasklistEventStore,
}

impl TaskCreator {
    pub fn new(event_store: TasklistEventStore) -> Self {
        TaskCreator { event_store }
    }

    pub fn create_task(&self, tasklist_id: &str, title: &str) -> Result<(), TasklistError> {
        let command = TasklistCommand::CreateTask {
            title: title.to_string(),
        };

        let tasklist = TasklistService::new();
        tasklist.handle(tasklist_id, command, &self.event_store)
    }
}

pub struct TaskCompleter {
    event_store: TasklistEventStore,
}

impl TaskCompleter {
    pub fn new(event_store: TasklistEventStore) -> TaskCompleter {
        TaskCompleter { event_store }
    }

    pub fn complete_task(&self, tasklist_id: &str, task_id: u32) -> Result<(), TasklistError> {
        let tasklist = TasklistService::new();

        let command = TasklistCommand::CompleteTask {
            tasklist_id: tasklist_id.to_string(),
            task_id,
        };

        tasklist.handle(tasklist_id, command, &self.event_store)
    }
}
