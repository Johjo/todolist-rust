use crate::domain::event_store::AggregateId;
use crate::domain::tasklist::aggregate::TaskListAggregate;
use crate::domain::tasklist::command::TasklistCommand;
use crate::domain::tasklist::error::TasklistError;
use crate::domain::tasklist::use_case::TasklistEventStore;

pub(crate) struct TasklistService {}

impl TasklistService {
    pub(crate) fn new() -> TasklistService {
        TasklistService {}
    }

    pub(crate) fn handle(
        &self,
        tasklist_id: &str,
        command: TasklistCommand,
        event_store: &TasklistEventStore,
    ) -> Result<(), TasklistError> {
        let aggregate = self.load_aggregate(tasklist_id, event_store);

        let events_content = aggregate.handle(command)?;

        event_store
            .appends_many_events(events_content, AggregateId::new(tasklist_id))
            .unwrap();

        Ok(())
    }

    fn load_aggregate(
        &self,
        tasklist_id: &str,
        event_store: &TasklistEventStore,
    ) -> TaskListAggregate {
        let history = event_store.list_events(AggregateId::new(tasklist_id));
        TaskListAggregate::new(history)
    }
}
