use crate::domain::event_store::{Event, EventContent};
use serde_json;
use std::collections::HashMap;
use std::sync::Mutex;

pub struct StorageInMemory {
    events: Mutex<HashMap<String, Vec<String>>>,
}

impl StorageInMemory {
    pub fn new() -> StorageInMemory {
        StorageInMemory {
            events: Mutex::new(HashMap::new()),
        }
    }

    pub fn append_event<EVENT: EventContent>(&self, aggregate_id: String, event: Event<EVENT>) {
        let serialized = serde_json::to_string(&event).unwrap();

        let mut events = self.events.lock().unwrap();

        events
            .entry(aggregate_id)
            .or_insert(vec![])
            .push(serialized);
    }

    pub fn list_event<EVENT: EventContent>(&self, aggregate_id: String) -> Vec<Event<EVENT>> {
        // let events_as_string = self.events_hashmap2.get(&aggregate_id).unwrap_or(&vec![]).clone();
        // let events: Vec<EVENT> = events_as_string.iter().map(|s| serde_json::from_str(s).unwrap()).collect();

        let existing_events = self.events.lock().unwrap();

        let events: Vec<Event<EVENT>> = existing_events
            .get(&aggregate_id)
            .unwrap_or(&vec![])
            .iter()
            .map(|s| serde_json::from_str(s).unwrap())
            .collect();

        return events;
    }
}
